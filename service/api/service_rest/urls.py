from django.urls import path

from .views import list_technicians, detail_technicans, list_appointments, detail_appointments, update_appointment_cancel, update_appointment_finished

urlpatterns = [
    # get and post technicians
    path("technicians/", list_technicians, name="list_technicians"),
    # delete technician url
    path("technicians/<int:id>/", detail_technicans, name="detail_technicians"),
    # get and post appointments
    path("appointments/", list_appointments, name="list_appointments"),
    # delete appointment url
    path("appointments/<int:id>/", detail_appointments, name="detail_appointments"),
    # cancel update appointment
    path("appointments/<int:id>/cancel/", update_appointment_cancel, name="update_appointment_cancel"),
    # finish update appointment
    path("appointments/<int:id>/finish/", update_appointment_finished, name="update_appointment_finished"),
]
