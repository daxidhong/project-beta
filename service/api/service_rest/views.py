from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
    def get_extra_data(self, o):
        count = AutomobileVO.objects.filter(vin=o.vin).count()
        return {"vip": count > 0}

# Create your views here.

@require_http_methods (["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technicians": technicians}, encoder=TechnicianEncoder)
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)

@require_http_methods (["DELETE"])
def detail_technicans(request, id):
    count, _ = Technician.objects.filter(id=id).delete()
    return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse({"appointments": appointments}, encoder=AppointmentEncoder)
    else:
        content = json.loads(request.body)
        try:
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "This technician doesn't exist!"}, status=400)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)

@require_http_methods(["DELETE"])
def detail_appointments(request, id):
    count, _ = Appointment.objects.filter(id=id).delete()
    return JsonResponse({"deleted": count > 0})

# update cancel
@require_http_methods(["PUT"])
def update_appointment_cancel(request, id):
    appointment = Appointment.objects.filter(id=id).update(status="cancelled")
    return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)

# update finish
@require_http_methods(["PUT"])
def update_appointment_finished(request, id):
    appointment = Appointment.objects.filter(id=id).update(status="finished")
    return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
