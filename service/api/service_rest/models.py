from django.db import models


# Create your models here.
class Technician (models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=100, unique=True)


class AutomobileVO (models.Model):
    vin = models.CharField(max_length=200)
    import_href = models.CharField(max_length=200, null=True, unique=True)


class Appointment (models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(max_length=200)
    vin = models.CharField(max_length=200, unique=True)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.date_time

    class Meta:
        ordering = ("date_time",)
