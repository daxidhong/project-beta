import React, {useState, useEffect} from 'react';

export default function AutomobileList() {

  const[automobiles, setAutomobiles] = useState([]);

  const fetchData = async () => {
    const autoUrl = 'http://localhost:8100/api/automobiles/';
    const response = await fetch(autoUrl);
    if (response.ok) {
      const data = await response.json();
    //   "autos" is from views.py under the GET req
      setAutomobiles(data.autos);
    }
  }

  useEffect(() => {
    fetchData();
}, []);

  return (
    <div>
      <h1>Automobiles</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map(automobile => {
            return (
              <tr key={automobile.vin}>
                <td>{automobile.vin}</td>
                <td>{automobile.color}</td>
                <td>{automobile.year}</td>
                <td>{automobile.model.name}</td>
                <td>{automobile.model.manufacturer.name}</td>
                <td>{automobile.sold ? 'Yes' : 'No'}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  );
}
