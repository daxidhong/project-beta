import React, {useState} from 'react';

export default function TechnicianForm() {
    const[first, setFirst] = useState('');
    const[last, setLast] = useState('');
    const[employeeId, setEmployeeId] = useState('');

    const handleFirstChange = (event) => {
        const value = event.target.value;
        setFirst(value);
    }
    const handleLastChange = (event) => {
        const value = event.target.value;
        setLast(value);
    }
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first;
        data.last_name = last;
        data.employee_id = employeeId;


        const url = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const data = await response.json();
            setFirst('');
            setLast('');
            setEmployeeId('');
        }
    };

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add a technician</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                <input onChange={handleFirstChange} value={first} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control"></input>
                <label htmlFor="first_name">First name...</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleLastChange} value={last} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control"></input>
                <label htmlFor="last_name">Last name...</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleEmployeeIDChange} value={employeeId} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control"></input>
                <label htmlFor="employee_id">Employee ID...</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    )
}
