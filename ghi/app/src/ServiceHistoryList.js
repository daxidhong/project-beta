import React, { useState, useEffect } from 'react';

export default function ServiceHistoryList() {
    const[services, setServices] = useState([]);
    const[filteredSearch, setFilteredSearch] = useState([]);
    const[search, setSearchInput] = useState('');

    const fetchData = async () => {
    const url = 'http://localhost:8080/api/appointments/'
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setServices(data.appointments);
        }
    }
    useEffect (() => {
        const filtered = services.filter(service => {
            const vin = service.vin.toLowerCase();
            return vin.startsWith(search.toLowerCase());
        });
        setFilteredSearch(filtered);
        }, [search, services]
    );

    useEffect (() => {
        fetchData();
    }, []);

    return (
    <div>
        <div>
            <h1>Service History</h1>
        </div>
        <div>
        <label>
            <input placeholder="Search by VIN..." type="text" value={search} onChange={event => setSearchInput(event.target.value)} />
        </label>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date and Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {filteredSearch.map(service => (
                <tr key={service.id}>
                    <td>{service.vin}</td>
                    <td>{service.vip ? "Yes" : "No"}</td>
                    <td>{service.customer}</td>
                    <td>{new Date(service.date_time).toLocaleDateString("en-US")} {new Date(service.date_time).toLocaleTimeString("en-US")}</td>
                    <td>{service.technician.first_name} {service.technician.last_name}</td>
                    <td>{service.reason}</td>
                    <td>{service.status}</td>
                </tr>
                ))}
            </tbody>
        </table>
    </div>
    );
}
