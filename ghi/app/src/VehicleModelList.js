import React, { useState, useEffect } from 'react'

export default function VehicleList(){
    const[vehicles, setVehicles] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json();
            // "models" is from views.py GET req
            setVehicles(data.models)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
            {vehicles.map((vehicle) =>
                <tr key={vehicle.id}>
                    <td>{vehicle.name}</td>
                    <td>{vehicle.manufacturer.name}</td>
                    <td> <img src={vehicle.picture_url} /> </td>
                </tr>
            )}
                </tbody>
            </table>
        </div>
    )
}
