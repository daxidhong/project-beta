import React, {useEffect, useState} from "react";

function CreateSalesperson() {
    
    const[formData, setFormData] = useState(
        {
            first_name: '',
            last_name: '',
            employee_id: '',
        }
    );

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            });
        }
    }

    return(
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create Salesperson</h1>
                <form onSubmit={handleSubmit} id="create-salesperson-form">

                    <div className="form-floating mb-3">
                        <input value={formData.first_name} onChange={handleFormChange} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control"/>
                        <label htmlFor="name">First Name...</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={formData.last_name} onChange={handleFormChange} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control"/>
                        <label htmlFor="name">Last Name...</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={formData.employee_id} onChange={handleFormChange} placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control"/>
                        <label htmlFor="name">Employee ID...</label>
                    </div>

                    <button className="btn btn-primary"> Create </button>
                </form>
            </div>
        </div>
        </div>
    )
}

export default CreateSalesperson





















































//     const [salesperson, setSalesperson] = useState('');
//     const [first_name, setFirstName] = useState('');
//     const [last_name, setLastName] = useState('');
//     const [employee_id, setEmployeeId] = useState('');


//     const fetchData = async () => {
//         const response = await fetch('http://localhost:8090/api/salespeople/');
//             if (response.ok) {
//                 const data = await response.json();
//                 setSalesperson(data.salesperson)
//             }
//     }

//     useEffect(() => {
//         fetchData();
//     }, []);


//     const handleFirstNameChange = (e) => {
//         const value = e.target.value;
//         setFirstName(value);
//     }

//     const handleLastNameChange = (e) => {
//         const value = e.target.value;
//         setLastName(value);
//     }

//     const handleEmployeeID = (e) => {
//         const value = e.target.value;
//         setEmployeeId(value);
//     }

//     const handleSubmit = async (event) => {
//         event.preventDefault();
//         const data = {}

//         data.first_name = first_name
//         data.last_name= last_name
//         data.employee_id= employee_id

//         const url = 'http://localhost:8090/api/salespeople';
//         const fetchConfig = {
//             method: "post",
//             body: JSON.stringify(data),
//             header: {
//                 'Content-Type': 'application/json',
//             },
//         };

//         const response = await(url, fetchConfig);

//         if (response.ok) {
//             const newSalesperson = await response.json()
//             setFirstName('');
//             setLastName('');
//             setEmployeeId('');
//         }
//     }

//     return (
//         <div className="row">
//         <div className="offset-3 col-6">
//             <div className="shadow p-4 mt-4">
//                 <h1>Add a Salesperson</h1>
//                 <form onSubmit={handleSubmit} id="create-customer-form">
//                     <div className="form-floating mb-3">
//                         <input onChange={handleFirstNameChange} placeholder="First_name" required type="text" name="first_name" id="first_name" className="form-control" ></input>
//                         <label htmlFor="first_name">First name...</label>
//                     </div>
//                     <div className="form-floating mb-3">
//                         <input onChange={handleLastNameChange} placeholder="Last_name" required type="text" name="last_name" id="last_name" className="form-control" ></input>
//                         <label htmlFor="last_name">Last name...</label>
//                     </div>
//                     <div className="form-floating mb-3">
//                         <input onChange={handleEmployeeID} placeholder="Employee_id" required type="text" name="employee_id" id="employee_id" className="form-control" ></input>
//                         <label htmlFor="address">Employee ID...</label>
//                     </div>
//                     <button className="btn btn-primary"> Create </button>

//                 </form>
//             </div>
//         </div>
//         </div>
//     )

// }

// export default CreateSalesperson;