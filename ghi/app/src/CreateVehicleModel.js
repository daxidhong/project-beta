import React, { useEffect , useState } from 'react'

export default function VehicleForm() {
    const[manufacturers, setManufacturers] = useState([]);
    const[name, setName] = useState('');
    const[pictureUrl, setPictureUrl] = useState('');
    const[manufacturer, setManufacturer] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const handlePictureChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}

        data.name = name
        data.picture_url = pictureUrl
        data.manufacturer_id = manufacturer

        const url = "http://localhost:8100/api/models/"
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            header: {
                'Content-Type':'application/json'
            },
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newVehicle = await response.json()
            setName('');
            setPictureUrl('');
            setManufacturer('');
        }
    }

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
            if (response.ok){
                const data = await response.json()
                setManufacturers(data.manufacturers)
            }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a New Vehicle Model!</h1>
            <form onSubmit={handleSubmit} id="create-vehicle-model-form">
                <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"></input>
                <label htmlFor="name">Model name...</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handlePictureChange} value={pictureUrl} placeholder="Picture" required type="url" name="pictureUrl" id="pictureUrl" className="form-control"></input>
                <label htmlFor="pictureUrl">Picture URL...</label>
                </div>
                <div className="mb-3">
                <select onChange={handleManufacturerChange} value={manufacturer} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                    <option value="">Choose a manufacturer...</option>
                    {manufacturers.map(manufacturer => {
                            return (
                            <option key={manufacturer.href} value={manufacturer.id}>
                                {manufacturer.name}
                            </option>
                            );
                        })}
                </select>
                </div>
                <button className="btn btn-primary">Create Vehicle</button>
            </form>
            </div>
        </div>
        </div>
    )
}
