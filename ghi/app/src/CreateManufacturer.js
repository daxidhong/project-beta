import React, { useState } from 'react';

function CreateManufacturer() {
    const[formData, setFormData] = useState(
        {
            name: '',
        }
    );

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
            });
        }
    }

    return(
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create Manufacturer</h1>
                <form onSubmit={handleSubmit} id="create-manufacturer-form">

                    <div className="form-floating mb-3">
                        <input value={formData.name} onChange={handleFormChange} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                        <label htmlFor="name">Manufacturer</label>
                    </div>

                    <button className="btn btn-primary"> Create Manufacturer</button>
                </form>
            </div>
        </div>
        </div>
    )
}

export default CreateManufacturer;
