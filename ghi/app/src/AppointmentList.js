// // contain relevant list (vin, customer name, date/time, technician, reason)
// // DON'T FORGET: ADD CLICK BUTTONS FOR FINISH AND COMPLETE
import React, {useState, useEffect} from 'react';

export default function AppointmentList() {
    const[appointments, setAppointments] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleCancelChange = async (event) => {
        const url = `http://localhost:8080/api/appointments/${event}/cancel/`;
        const fetchConfig = {
            method: 'put',
        }
        const response = await fetch (url, fetchConfig);
            if (response.ok) {
                fetchData();
            }
        }

    const handleFinishChange = async (event) => {
        const url = `http://localhost:8080/api/appointments/${event}/finish/`;
        const fetchConfig = {
            method: 'put',
        }
        const response = await fetch (url, fetchConfig);
        if (response.ok) {
            fetchData();
        }
    }

    return (
        <div>
          <h1>Service Appointments</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date and Time</th>
                <th>Technician</th>
                <th>Reason</th>
              </tr>
            </thead>
            <tbody>
              {appointments.map(appointment => {
                return (
                  <tr key={appointment.id}>
                    <td>{appointment.vin}</td>
                    <td>{appointment.vip ? "Yes" : "No"}</td>
                    <td>{appointment.customer}</td>
                    <td>{appointment.date_time}</td>
                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                    <td>{appointment.reason}</td>
                        <div>
                            <button onClick={() => handleCancelChange(appointment.href)} className="btn btn-danger">Cancel</button>
                            <button onClick={() => handleFinishChange(appointment.href)} className="btn btn-success">Finish</button>
                        </div>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      );
}
