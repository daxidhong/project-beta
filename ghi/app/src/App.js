import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import VehicleModelList from './VehicleModelList';
import CreateVehicleModel from './CreateVehicleModel';
import AutomobileList from './AutomobileList';
import ManufacturerList from './ManufacturerList';
import CreateManufacturer from './CreateManufacturer';
import CreateAutomobile from './CreateAutomobile';
import CustomerList from './CustomerList';
import CreateCustomer from './CreateCustomer';
import SalesList from './SalesList';
import CreateSale from './CreateSale';
import SalespeopleList from './SalespeopleList';
import CreateSalesperson from './CreateSalesperson';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import ServiceHistoryList from './ServiceHistoryList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="vehicles" element={<VehicleModelList />} />
          <Route path="vehicles/new" element={<CreateVehicleModel />} />
          <Route path="automobiles" element={<AutomobileList />} />
          <Route path="automobiles/new" element={<CreateAutomobile/>} />
          <Route path="manufacturers" element={<ManufacturerList/>} />
          <Route path="manufacturers/new" element={<CreateManufacturer/>} />
          <Route path="customer" element={<CustomerList/>}/>
          <Route path="customer/new" element={<CreateCustomer/>}/>
          <Route path="sales" element={<SalesList/>}/>
          <Route path="sales/new" element={<CreateSale/>}/>
          <Route path="salesperson" element={<SalespeopleList/>}/>
          <Route path="salesperson/new" element={<CreateSalesperson/>}/>
          <Route path="technicians" element={<TechnicianList />} />
          <Route path="technicians/new" element={<TechnicianForm />} />
          <Route path="appointments" element={<AppointmentList />} />
          <Route path="appointments/new" element={<AppointmentForm />} />
          <Route path="history" element={<ServiceHistoryList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
