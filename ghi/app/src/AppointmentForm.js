//contain relevant inputs (vin, owner name, appointment date/time, technician select, reason)
import { useState, useEffect } from "react"

export default function AppointmentForm() {
    const[technicians, setTechnicians] = useState([])
    const[appointmentData, setAppointmentForm] = useState({
        date:'',
        time:'',
        vin:'',
        customer:'',
        technician:'',
        reason:'',
    })

    const handleAppointemntFormChange = (event) => {
        const value = event.target.value;
        const input = event.target.name;
        setAppointmentForm ({
            ...appointmentData,
            [input]:value
        });
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const {date, time, ...appointmentForm} = appointmentData;
        const dateTime = `${date} ${time}`;

        const url = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify ({...appointmentForm, date_time: dateTime}),
            headers: {
                'Content-Type': "application/json"
            },
        };


        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setAppointmentForm ({
                date:'',
                time:'',
                vin:'',
                customer:'',
                technician:'',
                reason:'',
            });
        }
    };

    const fetchData = async() => {
        const response = await fetch('http://localhost:8080/api/technicians/')
        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }

    useEffect( ()=> {
        fetchData()
    }, []);

    return(
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create an Appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">

                <div className="form-floating mb-3">
                <input onChange={handleAppointemntFormChange} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control" value={appointmentData.vin}/>
                <label htmlFor="vin">Automobile VIN</label>
                </div>

                <div className="form-floating mb-3">
                <input onChange={handleAppointemntFormChange} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" value={appointmentData.customer}/>
                <label htmlFor="customer">Customer</label>
                </div>

                <div className="form-floating mb-3">
                <input onChange={handleAppointemntFormChange} required type="date" name="date" id="date" className="form-control" value={appointmentData.date}/>
                <label htmlFor="date">Date</label>
                </div>

                <div className="form-floating mb-3">
                <input onChange={handleAppointemntFormChange} required type="time" name="time" id="time" className="form-control" value={appointmentData.time}/>
                <label htmlFor="time">Time</label>
                </div>

                <div className="form-floating mb-3">
                    <select onChange={handleAppointemntFormChange} required name="technician" id="technician" className="form-select" value={appointmentData.technician}>
                        <option disabled value="">Choose a technician...</option>
                        {technicians.map(technician => {
                        return (
                            <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                        )
                        })}
                    </select>
                </div>

                <div className="form-floating mb-3">
                <input onChange={handleAppointemntFormChange} required type="text" name="reason" id="reason" className="form-control" value={appointmentData.reason}/>
                <label htmlFor="reason">Reason</label>
                </div>

                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    )
};
