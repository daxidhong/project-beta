
Team:
* Person 1 - David Hong - Service
* Person 2 - Josh Tobin - Sales

## Project Diagram
![Project Diagram](CarCar-Diagram.png)


## Run this Project - Getting Started
1. One person forks the repository at: https://gitlab.com/sjp19-public-resources/sjp-2022-april/project-beta
2. That person invites the team member's GitLab username as a maintainer of the forked repository.
3. Then, both team members ```cd``` into the desired directory to clone the forked repository.
4. To clone, run ```git clone https://gitlab.com/<ENTER_USERNAME>/project-beta.git```
5. After both team members clone the same repository that was just forked then run the following commands:
    ```
    docker volume create beta-data
    docker-compose build
    docker-compose up
    ```
6. Decide who does what microservice (Service or Sales).

## URLs and Ports for the Project
```
Front End:
    http://localhost:3000/
    Port: 3000

Inventory:
    http://localhost:8100/api/
    Port: 8100

Service:
    http://localhost:8080/api/

Sales:
    http://localhost:8090/api/
    Port: 8090
```
## Inventory (PORT: 8100)
* The inventory microservice is a shared responsibility between teammates.
* Create React components for the following:
    * Manufacturers:
        * Show a list of manufacturers
        * Create a manufacturer
    * VehicleModel:
        * Show a list of vehicle models
        * Create a vehicle model
    * Automobiles:
        * Show a list of automobiles in inventory
        * Create an automobile in inventory
#### Manufactuers
| Action | Method | URL |
|---|---|---|
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create manufacturer | POST | http://localhost:8100/api/manufacturers/
| Get specific manufacturer | GET | http://localhost:8100/api/manufacturers/:id/
| Update specific manufacturer | PUT | http://localhost:8100/api/manufacturers/:id/
| Delete specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/

* Sample GET output:
```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Toyota"
		}
	]
}
```
* Sample POST input and output:
```
input:
{
  "name": "Toyota"
}

output:
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Toyota"
}
```
* Sample PUT input and output:
```
input:
{
  "name": "Nissan"
}

output:
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Nissan"
}
```
* Sample DELETE output:
```
{
	"id": null,
	"name": "Nissan"
}
```
#### VehicleModel
| Action | Method | URL |
|---|---|---|
| List vehicle models | GET | http://localhost:8100/api/models/
| Create vehicle model | POST | http://localhost:8100/api/models/
| Get specific vehicle model | GET | http://localhost:8100/api/models/:id/
| Update specific vehicle model | PUT | http://localhost:8100/api/models/:id/
| Delete specific vehicle model | DELETE | http://localhost:8100/api/models/:id/

* Sample GET output:
```
{
	"models": [
		{
			"href": "/api/models/2/",
			"id": 2,
			"name": "Ferrari",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/2/",
				"id": 2,
				"name": "Toyota"
			}
		}
	]
}
```
* Sample POST input and output:
```
input:
{
  "name": "Ferrari",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 2
}

output:
{
	"href": "/api/models/2/",
	"id": 2,
	"name": "Ferrari",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/2/",
		"id": 2,
		"name": "Toyota"
	}
}
```
* Sample PUT input and output:
```
input:
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}

output:
{
	"href": "/api/models/2/",
	"id": 2,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/2/",
		"id": 2,
		"name": "Toyota"
	}
}

```
* Sample DELETE output:
```
{
	"id": null,
	"name": "Ferrari",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/2/",
		"id": 2,
		"name": "Toyota"
	}
}
```

#### VehicleModel

| Action | Method | URL |
|---|---|---|
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get specific automobile | GET | http://localhost:8100/api/automobiles/:vin/
| Update specific automobile | PUT | http://localhost:8100/api/automobiles/:vin/
| Delete specific automobile | DELETE | http://localhost:8100/api/automobiles/:vin/

* Sample GET output:
```
{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120174/",
			"id": 3,
			"color": "red",
			"year": 2012,
			"vin": "1C3CC5FB2AN120174",
			"model": {
				"href": "/api/models/4/",
				"id": 4,
				"name": "Ferrari",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/2/",
					"id": 2,
					"name": "Toyota"
				}
			},
			"sold": false
		}
	]
}
```
* Sample POST input and output:
```
input:
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 4
}

output:
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 3,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/4/",
		"id": 4,
		"name": "Ferrari",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Toyota"
		}
	},
	"sold": false
}
```
* Sample PUT input and output:
```
input:
{
  "color": "blue",
  "year": 2020,
  "sold": true
}

output:
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 3,
	"color": "blue",
	"year": 2020,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/4/",
		"id": 4,
		"name": "Ferrari",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Toyota"
		}
	},
	"sold": true
}
```
* Sample DELETE output:
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": null,
	"color": "blue",
	"year": 2020,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/4/",
		"id": 4,
		"name": "Ferrari",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Toyota"
		}
	},
	"sold": true
}
```

## Service microservice (PORT: 8080)
* The Service microservice keeps track of service appointments and their owners

I created three models:
1. Technician model
    - The Technician model takes three properties:
        - First name
        - Last name
        - Employee ID
    - The Technician model will be used to create a technicians, which can then be viewed in the list of technicians
    - The Technician model allows us to create technicians with those given properties, and then get assigned to an appointment, as seen in the foreign key in the Appointment model.

2. AutomobileVO model
    - The AutomobileVO model takes two properties:
        - VIN number
        - import_href
    - Automobiles are made into value objects, which are immuntable and represent a value that does not have identity beyond its value
    - We create a poller that polls value objects (automobile VOs) because pollers constantly retrieve current state of values

3. Appointment model
    - The Appointment model takes six properties:
        - Date/Time of appointment
        - Reason for appointment
        - Status of appointment
        - VIN number of vehicle
        - Customer's name
        - Technician for the appointments, which is a foreign key
    - We add a technician as a foreign key in the Appointment model so that we ensure that there is a technician assigned for that appointment to take care of a customer's automobile.

- After the models were created, we registered them in the admin.py and made a poller called get_auto to poll automobile value objects.


| Action | Method | URL |
|---|---|---|
| List technicians | GET | http://localhost:8080/api/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete specific technician | DELETE | http://localhost:8080/api/technicians/:id
| List appointments | GET | http://localhost:8080/api/appointments/
| Create an appointment | POST | http://localhost:8080/api/appointments/
| Delete an appointment | DELETE | http://localhost:8080/api/appointments/:id
| Update appointment to cancelled | PUT | http://localhost:8080/api/appointments/:id/cancel
| Update appointment to finished | PUT | http://localhost:8080/api/appointments/:id/finish

* Sample GET output for list of technicians:
```
{
	"technicians":
		{
			"first_name": "LeBron",
			"last_name": "James",
			"employee_id": "23",
			"id": 1
		}
}
```
* Sample POST input and output for technicians:
```
input:
{
  "first_name": "LeBron",
  "last_name": "James",
  "employee_id": "23"
}

output:
{
	"first_name": "LeBron",
	"last_name": "James",
	"employee_id": "23",
	"id": 1
}
```
*Sample DELETE output for specific technician:
```
{
	"deleted": true
}
```
* Sample GET output for list of appointments:
```
{
	"appointments":
		{
			"id": 4,
			"date_time": "2023-04-04T01:15:00+00:00",
			"reason": "Ran out of blinker fluid",
			"status": "cancelled",
			"vin": "TESTESTEST",
			"customer": "David",
			"technician": {
				"first_name": "James",
				"last_name": "Harden",
				"employee_id": "Phila",
				"id": 3
			},
			"vip": false
		}
}
```
* Sample POST input and output for appointments:
```
input:
{
  "date_time": "2023-05-16T09:30:00",
  "reason": "Windshield",
	"vin": "1C3CC5FB2AN120174",
	"technician": 1
}

output:
{
	"id": 2,
	"date_time": "2023-05-16T09:30:00",
	"reason": "Windshield",
	"status": "",
	"vin": "1C3CC5FB2AN120174",
	"customer": "",
	"technician": {
		"first_name": "LeBron",
		"last_name": "James",
		"employee_id": "23",
		"id": 1
	}
}
```
* Sample DELETE output for specific appointment:
```
{
	"deleted": true
}
```
* Sample PUT output for cancelled and finished appointments:
```
cancelled:
{
	"appointments":
		{
			"id": 4,
			"date_time": "2023-04-04T01:15:00+00:00",
			"reason": "Ran out of blinker fluid",
			"status": "cancelled",
			"vin": "TESTESTEST",
			"customer": "David",
			"technician": {
				"first_name": "James",
				"last_name": "Harden",
				"employee_id": "Phila",
				"id": 3
			},
			"vip": false
		}
}

finished:
{
	"appointments":
		{
			"id": 4,
			"date_time": "2023-04-04T01:15:00+00:00",
			"reason": "Ran out of blinker fluid",
			"status": "finished",
			"vin": "TESTESTEST",
			"customer": "David",
			"technician": {
				"first_name": "James",
				"last_name": "Harden",
				"employee_id": "Phila",
				"id": 3
			},
			"vip": false
		}
}

```
## Sales microservice (PORT:8090)
*The Sales microservice keeps track of automobile sales that comes from the inventory.

Three models were created:
1. Salesperson model
    -Contains these three fields.
        -first_name
        -last_name
        -employee_id

2. Customer model
    -contains these 4 fields.
        -first_name
        -last_name
        -address
        -phone_number

3. Sale model
    -contains these 4 fields.
        -automobile
        -salesperson
        -customer
        -price
    -Automobile, Salesperson, and Customer were all created as foreign keys.

4. AutomobileVO
    -a model containing a vin field.
    
-After the models were created they were registered in the admin.py file
-A poller was also created as get_sales to poll automobile Vehicle Identification Numbers


| Action | Method | URL |
|---|---|---|
| List salespeople | GET | http://localhost:8090/api/salepeople/
| Create a salespeople | POST | http://localhost:8090/api/salespeople/
| Delete specific salespeople | DELETE | http://localhost:8090/api/salepeople/:id
| List customer | GET | http://localhost:8090/api/customers/
| Create an customer | POST | http://localhost:8090/api/customers/
| Delete an specific customer | DELETE | http://localhost:8090/api/customers/:id
| List sales | GET | http://localhost:8090/api/sales/
| Create a sale  | POST | http://localhost:8090/api/sales/
| Delete a sale  | DELETE | http://localhost:8090/api/sales/:id

-Each endpoint should return a 400 or 404 error if unsuccessful or attempting to access a model object that does not exist.Each endpoint should return a 400 or 404 error if unsuccessful or attempting to access a model object that does not exist.

* Sample GET output for list of salespeople:
```
{
		"salespeople":
		{
			"id": 2,
			"first_name": "Mike",
			"last_name": "Trout",
			"employee_id": "Mtrout"
		},
}
```
* Sample POST input and output for salespople:
```
input:
{
  "first_name": "Torii",
  "last_name": "Hunter",
  "employee_id": "thunter"
}

output:
{
	"id": 6,
	"first_name": "Torii",
	"last_name": "Hunter",
	"employee_id": "thunter"
}
```
*Sample DELETE output for specific salespeople:
```
{
	"deleted": true
}
```
* Sample GET output for list customers:
```
{
	"customers":
		{
			"id": 2,
			"first_name": "Andy",
			"last_name": "Dufrense",
			"address": "23342 Shawshank BLVD, Boston, MA",
			"phone_number": "555-323-4224"
		}
}
```
* Sample POST input and output for customers:
```
input:
{
  "first_name": "Bob",
  "last_name": "Melvin",
  "address": "3333 Oakland St. Los Angeles, CA",
	"phone_number": 5553248873
}

output:
{
	"id": 5,
	"first_name": "Bob",
	"last_name": "Melvin",
	"address": "3333 Oakland St. Los Angeles, CA",
	"phone_number": 5553248873
}
```
* Sample DELETE output for specific customers:
```
{
	"deleted": true
}
```
* Sample GET output for Sales:
```

{
	{
	"vin": "test",
	"salesperson": "Test",
	"customer": "Test",
	"price": "456",
    }
}

```
* Sample POST input and output for Sales:
```
input:
{
  "first_name": "Bob",
  "last_name": "Melvin",
  "address": "3333 Oakland St. Los Angeles, CA",
	"phone_number": 5553248873
}

output:
{
	"id": 1,
	"vin": "MHD234234D3221",
	"salesperson": "Jordan,
	"customer": "Curry, Steph",
	"price": 23422.00
}
```
* Sample DELETE output for specific customers:
```
{
	"deleted": true
}
```
